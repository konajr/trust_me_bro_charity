class Footer extends HTMLElement{
    connectedCallback(){
        this.innerHTML = '\
        <footer class="footer-width">\
            <div class="footer-content">\
                <div class="footer-section">\
                    <h3>Contact Us</h3>\
                    <p>Email: trustmebrocharity@gmail.com</p>\
                    <p>Phone: +91 98765 43210</p>\
                </div>\
                <div class="footer-section fs2">\
                    <h3>Follow Us</h3>\
                    <p><a href="https://twitter.com/TMB_Charity">Twitter</a> | <a href="https://www.facebook.com/profile.php?id=61555397318489">Facebook</a> | <a href="https://www.instagram.com/tmb_charity/">Instagram</a></p>\
                </div>\
                <div class="footer-section">\
                    <h3>Donate</h3>\
                    <p>Your generosity fuels our mission.</p>\
                    <p><a href="donate.html">Donate Now</a></p>\
                </div>\
            </div>\
            <p>&copy; 2024 Trust Me Bro Charity. All rights reserved.</p>\
        </footer>\
        '
    }
}

customElements.define("site-footer", Footer)